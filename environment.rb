PROJECT_NAME = "DeusOps230524"

SSH_PUB_KEY_PATH = "#{Dir.home}/.ssh/id_rsa.pub"

SSH_PUB_KEYS = [
  File.readlines(SSH_PUB_KEY_PATH).first.strip,
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDdgYzG1BB5PwqA4A++RnLVAX74ZNuqdraBku2VLdljHwn7VsCDWb96Bfxhc8EjuKV5Uq4tkClPT3/xoROB+Pt3xs+iZM4qQqNpjWYWfCpRPYCeLDy7Xh5tuBOt06rsNsEN8hWgATZ/454LxPbiz2/z1BAKp1k5fL+JwSm6iM+31SeAC4jQr4nBiRW85/ZIEthAJwfg4lg0RCiWSniAH9apWPMmSRAacgt4XATV7FuHB+3cFumbLB81ABH7BRMwNe5sdO5JSNQxbRELoN+wLC0EVK8O8EcijMZa2mdxTF773jaFN43N3r9Pj7nmMY48R2P38AShQhydvAMNwng2AHHGzl5+g3X7CRc+1PKMru1UQ2yvRg/M5UNTZMBpBBoyYUwG1zV73YkVKWkOxrvrAXAgGhavcyno/eZZlNwWagzlf2m8XFelgL+yKnFqql/gfzccPWTqlRUh3geVRIJ0vNQUlWHu5vypQNcec5okQzwP+sy6XDSfWJkw0lCyUGpn0EU= root@a7f23479dc8e",
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC2nKMPY9VZFyeNbEnrCokRb+GyZh/wb9xwVYI4Nl9noLSqG459aiuCvyjilKho3hbKtrOsLdjCloprvpWq9NuvcArJ2Lk50M996BPvBxKExlcbEW5leDFeNd+Ji59r6FQvmQ6OXPyTg3l0Cs4CNu6RAFx2w19kIZx21m+hrPaHKh8lnEgLdkytmR9MXuoB3uaCLspImJeL8bsn8NgONAKCJIc4tbKh1ZjjJhWUEnFRdUm1R11kJ3iRo/mOFU4/sNVogvJo3YAliyyQrGW/Njyc684YJerT1OOdTGRzlVslAPUIxuV942ttJMuB76ZkDD/3+LxDOQIvd2nowIhYB6rnjsJtBAZTBz3SpPwh66h6Ba5LoCZ4DmSP2421B5CWNZPwC7Kxd1ZhKjKxL+wfbW5p3jfuifl8UiTW+hTP3EIhp035G4adaR4VracI7b2smMz5lx44Ncb2igrmXpVZi2tIfWiSc8viRu6i8x1R7UAti0ZH7sVoc1s+TRZkJl0bM9s= gitlab-runner@ee4aceb20d13"
]

ENVIRONMENTS = {
  "STAGE" => [
    {
      "name" => "app",
      "amount" => 1,
      "network" => "192.168.56.",
      "address_start_from" => 2,
      "box" => "ubuntu/focal64",
      "cpus" => 1,
      "memory" => 1024,
      "provisions" => [
        "vagrant/install-docker.sh",
        "vagrant/saleor-platform.sh"
      ]
    }
  ],

  "PROD" => [
    {
      "name" => "app",
      "amount" => 1,
      "network" => "192.168.57.",
      "address_start_from" => 2,
      "box" => "ubuntu/focal64",
      "cpus" => 1,
      "memory" => 1024,
      "provisions" => [
        "vagrant/install-docker.sh",
        "vagrant/saleor-platform.sh"
      ]
    }
  ]
}